package zenika;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ColorApi {

    public static void main(String[] args){
        SpringApplication.run(ColorApi.class, args);
    }
}
